public class Rover {

    private int x,y;
    char orientation;
    private int limiteDepassement = 5;

    public Rover() {
        this.x = 1;
        this.y = 1;
        this.orientation = 'N';
    }

    public Rover(int x, int y, char orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public int[] getPositionRover() {
        int[] positionRover = {x,y};
        return positionRover;
    }

    public char getOrientation() {
        return orientation;
    }

    private void deplacer(char direction) {

        if((direction =='F' && this.orientation=='N') || (direction == 'B' && this.orientation == 'S')) y += 1;
        if((direction =='F' && this.orientation=='S') || (direction == 'B' && this.orientation == 'N')) y -= 1;

        if((direction =='B' && this.orientation=='E') || (direction == 'F' && this.orientation == 'O')) x -= 1;
        if((direction =='B' && this.orientation=='O') || (direction == 'F' && this.orientation == 'E')) x += 1;
        checkPosition();

    }

    private void tourner(char commandeDroiteGauche) {

        if(commandeDroiteGauche =='G' && this.orientation == 'N') this.orientation = 'O';
        else if(commandeDroiteGauche =='D' && this.orientation == 'N') this.orientation = 'E';

        else if(commandeDroiteGauche =='G' && this.orientation == 'S') this.orientation = 'E';
        else if(commandeDroiteGauche =='D' && this.orientation == 'S') this.orientation = 'O';

        else if(commandeDroiteGauche =='G' && this.orientation == 'E') this.orientation = 'N';
        else if(commandeDroiteGauche =='D' && this.orientation == 'E') this.orientation = 'S';

        else if(commandeDroiteGauche =='G' && this.orientation == 'O') this.orientation = 'S';
        else if(commandeDroiteGauche =='D' && this.orientation == 'O') this.orientation = 'N';

    }

    private void checkPosition(){
        if(x > limiteDepassement) x=1;
        if(y > limiteDepassement) y=1;

        if(x <= 0) x=5;
        if(y <= 0) y=5;
    }

    public void gestionCommande(char[] commandesEntree) {
        for(char cmd: commandesEntree){
            if(cmd == 'F') deplacer(cmd);
            if(cmd == 'B') deplacer(cmd);
            if(cmd == 'G') tourner(cmd);
            if(cmd == 'D') tourner(cmd);
        }
    }
}
